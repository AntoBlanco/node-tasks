const io = require("socket.io-client");

let host = 'http://localhost:3000';
if(process.env.NODE_ENV && process.env.NODE_ENV == 'production'){
    console.log("Cambiando host");
    host = 'https://calm-river-70670.herokuapp.com/';
}

let socket = io.connect(host,{reconnect: true});

socket.on('connect',function(){
    console.log("\n\nSocket connected from NodeJS\n\n");
});

module.exports = socket;