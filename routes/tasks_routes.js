const express = require('express');
let TasksController = require('../controllers/tasks');

let router = express.Router();

router.route('/tasks').get(TasksController.index).post(TasksController.create);

// router.route('/tasks/new').get(TasksController.new); //Rutas Grupales

router.get('/tasks/new',TasksController.new); // Rutas Unicas
router.route('/tasks/:id').get(TasksController.show).put(TasksController.update).delete(TasksController.destroy); //wildcard 
router.get('/tasks/:id/edit',TasksController.edit);
//router.route('*').get(TasksController.missing);

//MANEJO DE ESTADO DE ERROR
//router.route('/404').get(TasksController.missing);

module.exports = router;