'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
      /*, // ESTO PARA CREAR LLAVE FORANEAS DESDE EL INICIO D LA TABLA
      userId:{
        type: Sequelize.INTEGER,
        references: {
          model: 'User', //name of Target Model
          key: 'id' // Key in Target model that we're referencing
        }
      }*/
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Tasks');
  }
};