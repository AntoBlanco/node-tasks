'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    tittle: DataTypes.STRING,
    color: DataTypes.STRING
  }, {});
  Category.associate = function(models) {
    // associations can be defined here
    Category.belongsToMany(models.Task,{
      through: 'TaskCategories', //Tabla Asociativa
      //as: 'tasks',
      //targetKey: 'categoryId',
      foreignKey: 'categoryId',
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  };
  return Category;
};