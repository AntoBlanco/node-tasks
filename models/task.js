'use strict';
const socket = require('../realtime/client');

module.exports = (sequelize, DataTypes) => {

  const Task = sequelize.define('Task', {
    description: DataTypes.TEXT,
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: false,
      references: {
        model: 'Users',
        key: 'id'
      }
    }
  }, {});
  Task.associate = function(models) {
    // associations can be defined here
    Task.belongsTo(models.User,{//pertenencia
      as:'user',
      foreignKey: 'userId',
      //targetKey: 'userId'
    });
    Task.belongsToMany(models.Category,{
      through: 'TaskCategories', //Tabla Asociativa
      as: 'categories',
      //targetKey: 'taskId',
      foreignKey: 'taskId',
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
  };

  Task.afterCreate(function(task,options){
    socket.emit('new_task',task);
  });

  return Task;
};