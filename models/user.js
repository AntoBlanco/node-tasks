'use strict'; //hooks

const bcrypt = require("bcryptjs");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
    }
    },
    password_hash: DataTypes.STRING,
    password: DataTypes.VIRTUAL // variables virtuales para manejar dentro del modelo y no en la BD
  }, {});

  User.login = function(email,password){
    //buscar usuario
    return User.findOne({// Encuentra un elemento por el valor email en este caso
      where: {
        email
      }
    }).then(user=>{ // se genera la promesa y si es valido user 
      // metodo de instancia para la clase del modelo
      if(!user) return null;
      return user.authPassword(password).then(valid => valid ? user : null); // aplica la funcion authpass para verificar el passhash contra el que fue ingresado en el sistema
    });
  };

  User.prototype.authPassword = function(password){ //Se agrega una nueva funcion para verificar el password en el modelo
    return new Promise((res,rej)=>{ // se retorna la promesa contenida en una función por que bscryp es asincrono
      bcrypt.compare(password,this.password_hash,function(err,valid){ // encrypta el valor password recibido de la instancia usuario obtenida del resultado del query y lo compara con el password almacenado en BD
        if(err) return rej(err); //si existe error es decir no lo encuentra retorna una promesa rechazada con el error
        res(valid); // si coincide la promesa se retorna la confirmación.
      })
    });
  };

  User.associate = function(models) {//permite asociar las relaciones del modelo user con otras entidades
    // associations can be defined here
    //hasMany belongsTo --> relaciones
    User.hasMany(models.Task,{
      as: 'tasks',
      foreignKey: 'userId'
    });
  };
  User.beforeCreate(function(user, options){// funcion que se ejecuta antes de la clase USER
    return new Promise((res,rej)=>{// genera una nueva promesa para que pueda ejecutar las funciones internas sin problema
      if(user.password){// valida que no este vacio el password --> variable virtual
        bcrypt.hash(user.password, 10, function(error,hash){// ejecuta la encriptación
          user.password_hash = hash; //asigna el hash retornado por bcrypt y lo entrega a variable para registrar el campo
          res(); // entrega el resultado de la promesa
        })
      };
    });
  });
  return User;
};