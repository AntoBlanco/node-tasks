const Category = require('../models').Category;

module.exports = {
    new: function(req,res){
       res.render("categories/new");
    },
    create: function(req,res){
        Category.create({
            tittle: req.body.tittle,
            color:  req.body.color
        }).then(result=>{
            //res.json(result);
            res.redirect('/categories/'+result.id);
        }).catch(error=>{
            console.log(error);
            res.json(error);
        })
    },
    show:function(req,res){
        Category.findByPk(req.params.id).then(function(category){
            res.render('categories/show',{category});
        });
    },
    index: function(req,res){
       Category.findAll().then(function(categories){
            res.render("categories/index",{categories});
       });
    },
    edit: function(req,res){
        Category.findByPk(req.params.id).then(function(category){
            res.render('categories/edit',{category});
        });
    },
    update: function(req,res){
        Category.update({
            tittle: req.body.title,
            color: req.body.color
        },{
            where: {
                id: req.params.id
            }
        }).then(function(response){
            if(response)
                res.redirect('/categories/'+req.params.id);
            else
                res.sendStatus('404');
        });
            
    },
    destroy: function(req,res){
        Category.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(response){
            if(response==1)
                res.redirect('/categories');
            else
                res.sendStatus('404');
        })
    }
}