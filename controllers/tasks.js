const Task = require('../models').Task;
const User = require('../models').User;

module.exports = {
    missing: function(req,res){
        //res.status(404).render('tasks/404');
        res.status(404);

        // respond with html page
        if (req.accepts('html')) {
          res.render('tasks/404', { url: req.url, ip: req.ip });
          return;
        }
      
        // respond with json
        if (req.accepts('json')) {
          res.send({ error: 'Not found' });
          return;
        }
      
        // default to plain-text. send()
        res.type('txt').send('Not found');
    },
    index: function(req,res){
        Task.findAll().then(tasks=>{
            res.render('tasks/index',{tasks: req.user.tasks});
        })
    },
    show: function(req,res){
        //res.send(req.params.id);
        Task.findByPk(req.params.id,{
            include: [
                {
                    model: User,
                    as:'user'
                },
                'categories'
            ]
        }).then(function(task){
            //res.json(tasks);
            if(task!=null)
                res.render('tasks/show',{task});
            else
                res.sendStatus('404');
        })
    },
    destroy: function(req,res){
        Task.destroy({
            where: {
                id: req.params.id
            }
        }).then(function(response){
            if(response==1)
                res.redirect('/tasks');
            else
                res.sendStatus('404');
        })
    },
    edit: function(req,res){
        //res.send(req.params.id);
        Task.findByPk(req.params.id).then(function(task){
            if(task!=null)
                res.render('tasks/edit',{task});
            else
                res.sendStatus('404');
        })
    },
    create: function(req,res){
        Task.create({
            description: req.body.description,
            userId:  req.user.id
        }).then(result=>{
            //despues retorna el objeto creado para usar addCategories
            //res.json(result);
            let categoryIds = req.body.categories.split(",");
            result.addCategories(categoryIds).then(()=>{
                res.redirect('/tasks/'+result.id);
            });    
        }).catch(error=>{
            console.log(error);
            res.json(error);
        })
    },
    update: function(req, res){
        /*Task.update({description: req.body.description},{
            where:  {
                id: req.params.id
            }
        }).then(function(response){
            res.redirect('/tasks/'+req.params.id);
            //res.json(tasks);
        })*/
        //Busca la tarea , actualiza descripción y guarda, y con el objeto retornado por la promesa uso addCategories
        let task = Task.findByPk(req.params.id).then(task=>{
            task.description = req.body.description;
            task.save().then(()=>{
                let categoryIds = req.body.categories.split(",");
                task.addCategories(categoryIds).then(()=>{
                    res.redirect('/tasks/'+req.params.id);
                });
            });
        });
    },
    new: function(req,res){
        res.render('tasks/new');
    }
};