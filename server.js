const express = require('express');
//const sqlite3 = require('sqlite3');
const bodyParser = require('body-parser');
//const Sequelize = require('sequelize');
const methodOverride = require('method-override');
const session = require('express-session');
const socketio = require('socket.io');

//Middleware
const findUserMiddleware = require('./middlewares/find_user');
const authUserMiddleware = require('./middlewares/auth_user');

const app = express();

const tasksRoutes = require('./routes/tasks_routes');
const registrationsRoutes = require('./routes/registrations_routes');
const sessionsRoutes = require('./routes/sessions_routes');
const categoriesRoutes = require('./routes/categories_routes');

app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));
app.set('view engine', 'pug');

let sessionConfig = {
    secret: ['asdlekjaus75sxfqdq2gdas','sadrjd123478ta1jmd9834n19asn1vasd1'], //secret para encriptar las sesiones
    saveUninitialized: false, //Forces a session that is "uninitialized" to be saved to the store. A session is uninitialized when it is new but not modified
    resave: false //Fuerza el guardado la sesion siempre y cuando sea modificado, esto sobre cualquier petición
}

if(process.env.NODE_ENV && process.env.NODE_ENV == 'production'){
    sessionConfig['Store'] = new (require('connect-pg-simple')(session))();
}

app.use(session(sessionConfig));

//middleware
app.use(findUserMiddleware);
app.use(authUserMiddleware);

//rutas
app.use(tasksRoutes);
app.use(registrationsRoutes);
app.use(sessionsRoutes);
app.use(categoriesRoutes);

app.get('/',function(req,res){
    res.render('home',{user: req.user});
});

let server = app.listen(process.env.PORT || 3000);
let io = socketio(server);
let sockets = {};

let userCount= 0;

io.on('connection',function(socket){

    let userId= socket.request._query.loggeduser;
    if(userId) sockets[userId] = socket;

    //actualiza usuarios en tiempo real
    userCount++;
    console.log('someone connected');

    io.emit('count_updated',{count: userCount})

    socket.on('new_task',function(user){
        if(user.userId){
            let userSocket = sockets[user.userId];
            if(!userSocket) return;

            userSocket.emit('new_task',user);
        }
        
    });

    socket.on('disconnect',function(){

        Object.keys(sockets).forEach(userId=>{
            let s = sockets[userId];
            if(s.id == socket.id) sockets[userId] = null;
        }); 

        userCount--;
        io.emit('count_updated',{count: userCount});
    })
});

const client = require('./realtime/client');